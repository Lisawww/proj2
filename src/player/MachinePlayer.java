/* MachinePlayer.java */

package player;
import player.chessboard.*;

/**
 *  An implementation of an automatic Network player.  Keeps track of moves
 *  made by both players.  Can select a move for itself.
 */
public class MachinePlayer extends Player {
    
    private final int SELF, OPPO;
    private int searchDepth, DEFAULTDEPTH = 3;
    private ChessBoard chessBoard;

  // Creates a machine player with the given color.  Color is either 0 (black)
  // or 1 (white).  (White has the first move.)
  public MachinePlayer(int color) {
      myName = "MachinePlayer";
      SELF = color; OPPO = 1 - SELF;
      searchDepth = DEFAULTDEPTH;
      chessBoard = new ChessBoard(this);
  }

  // Creates a machine player with the given color and search depth.  Color is
  // either 0 (black) or 1 (white).  (White has the first move.)
  public MachinePlayer(int color, int searchDepth) {
      this(color);
      this.searchDepth = searchDepth;
  }

  // Returns a new move by "this" player.  Internally records the move (updates
  // the internal game board) as a move by "this" player.
  public Move chooseMove() {
      Move bestMove = chessBoard.bestMove();
      chessBoard.move(SELF, bestMove);
      return bestMove;
  }

  // If the Move m is legal, records the move as a move by the opponent
  // (updates the internal game board) and returns true.  If the move is
  // illegal, returns false without modifying the internal state of "this"
  // player.  This method allows your opponents to inform you of their moves.
  public boolean opponentMove(Move m) {
      return chessBoard.move(OPPO, m);
  }

  // If the Move m is legal, records the move as a move by "this" player
  // (updates the internal game board) and returns true.  If the move is
  // illegal, returns false without modifying the internal state of "this"
  // player.  This method is used to help set up "Network problems" for your
  // player to solve.
  public boolean forceMove(Move m) {
      return chessBoard.move(SELF, m);
  }
  
  /**
   * color() returns the color of the player (BLACK or WHITE).
   * @return the color of the player (BLACK or WHITE);
   */
  public int color() {return SELF;}
  
  /**
   * searchDepth returns the depth of search.
   * @return the depth of search.
   */
  public int searchDepth() {return searchDepth;}
}