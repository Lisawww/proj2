/* GameTree.java */

package player.chessboard;
import player.*;
import player.chessboard.ChessBoard.Chip;

/**
 * A class for searching and identifying the best move of machine player.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 *
 */
class GameTree {
    
    private ChessBoard board;
    private Move bestMove = new Move();
    private Paths paths;
    private final int SELF, OPPO;
    
    /** 
     * GameTree() constructs GameTree objects.
     * 
     * @param board refers to the board that used by machine player. 
     */
    protected GameTree(ChessBoard board) {
        this.board = board;
        paths = new Paths(board);
        SELF = board.player.color(); OPPO = 1 - SELF;
    }
    
    /**
     * search() returns the pair of {alpha, beta} after choosing move.
     * 
     * @param alpha a score that the computer knows with certainty it can achieve.
     * @param beta a guarantee that the opponent can achieve a score of beta or lower.
     * @param side distinguishes whether the chip is white or black.
     * @param searchDepth refers to the number of recursion of the search.
     * @return the pair of {alpha, beta} after choosing move.
     */
    private int[] search(int alpha, int beta, int side, int searchDepth) {// alpha-beta pruning.
        int[] original = {alpha, beta};
        int score;
        if (paths.hasNetwork(SELF)) {
            score = Integer.MAX_VALUE;
        } else if (paths.hasNetwork(OPPO)) {
            score = Integer.MIN_VALUE;
        } else if (searchDepth == 0) {
            score = evaluate();
        } else {
            for (Move possibleMove : board.generateMoves(side)) {
                board.move(side, possibleMove);
                int[] stepScore = search(original[0], original[1], 1 - side, searchDepth - 1);
                board.counterMove(side, possibleMove);
                if (side == SELF && stepScore[1] > original[0]) {
                    original[0] = (int) (stepScore[1] * 0.99);
                } else if (side == OPPO && stepScore[0] < original[1]) {
                    original[1] = (int) (stepScore[0] * 0.99);
                }
                if (original[0] >= original[1]) {break;}
            }
            return original;
        }
        
        if (side == OPPO) {
            original[1] = score;
        } else if (side == SELF) {
            original[0] = score;
        }
        return original;
    }
    
    /**
     * evaluate() gives an evaluation of the board and return the score.
     *  
     * @return the score of the board through evaluation. 
     */
    private int evaluate() {
        int connectSELF = 0, connectOPPO = 0;
        for (Chip chip : board.positions(SELF)) {
            int connections = board.getConnections(chip, SELF).length();
            connectSELF += Math.pow(connections, connections);
        }
        for (Chip chip : board.positions(OPPO)) {
            int connections = board.getConnections(chip, OPPO).length();
            connectOPPO += Math.pow(connections, connections);
        }
        return connectSELF - connectOPPO;
    }
    
    /**
     * selectMove() selects the best possible move for machine player.
     * 
     * @return the best possible move.
     */
    protected Move selectMove() {
        int[] original = {Integer.MIN_VALUE, Integer.MAX_VALUE};
        for (Move possibleMove : board.generateMoves(SELF)) {
            board.move(SELF, possibleMove);
            int[] stepScore = search(original[0], original[1], OPPO, board.player.searchDepth() - 1);
            board.counterMove(SELF, possibleMove);
            if (stepScore[1] > original[0]) {
                original[0] = stepScore[1];
                bestMove = possibleMove;
            }
        }
        return bestMove;
    }
}