/* IllegalMoveException.java */

package player.chessboard;
import player.Move;

/**
 * An exception class thrown when a illegal move is detected.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 *
 */
@SuppressWarnings("serial")
public class IllegalMoveException extends Exception {
	
	public IllegalMoveException(Move m) {
		super("Illegal move by MachinePlayer: " + m);
	}	
}