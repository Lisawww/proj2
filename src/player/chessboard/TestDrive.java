/* TestDrive.java */

package player.chessboard;
import player.*;
import player.chessboard.list.*;
import player.chessboard.ChessBoard.Chip;

/**
 * a class for testing methods in package player.chessboard.
 *  
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 *
 */
public class TestDrive {
    
    protected void testHasNetwork(ChessBoard board) {
        System.out.print("Testing hasNetwork() :");        
        boolean hasNetwork = (new Paths(board)).hasNetwork(1);
        if (hasNetwork) {
            System.out.println(" successful");
        } else {
            System.out.println(" failed");
        }
    }
    
    protected void testGetConnections(ChessBoard board) {
        for (Chip chip : board.positions(1)) {
            ChipList connections = board.getConnections(chip, 1);
            System.out.println(connections + " and its length is: " + connections.length());
        }
    }
    
    protected void testChooseMove() {
        ChessBoard board = new ChessBoard(new MachinePlayer(1));
        board.move(1, new Move(6, 1));
        board.move(1, new Move(0, 2));
        board.move(1, new Move(3, 2));
        board.move(1, new Move(1, 3));
        board.move(1, new Move(5, 3));
        board.move(1, new Move(3, 5));
        board.move(1, new Move(4, 5));
        board.move(1, new Move(6, 5));
        board.move(0, new Move(1, 1));
        board.move(0, new Move(2, 1));
        board.move(0, new Move(4, 1));
        board.move(0, new Move(5, 1));
        board.move(0, new Move(1, 6));
        board.move(0, new Move(5, 5));
        board.move(0, new Move(2, 6));
        
        System.out.println(board.bestMove());
    }
    
    public static void main(String[] args) {
        ChessBoard board = new ChessBoard(new MachinePlayer(0));
        board.move(1, new Move(0, 1));
        board.move(0, new Move(6, 4));
        board.move(1, new Move(0, 3));
        board.move(0, new Move(6, 6));
        board.move(1, new Move(0, 4));
        board.move(0, new Move(2, 7));
        board.move(1, new Move(4, 3));
        board.move(0, new Move(1, 6));
        board.move(1, new Move(6, 5));
        board.move(0, new Move(3, 5));
        board.move(1, new Move(4, 1));
        board.move(0, new Move(2, 4));
        board.move(1, new Move(2, 6));
        board.move(0, new Move(6, 1));
        board.move(1, new Move(4, 5));
        board.move(0, new Move(5, 0));
        board.move(1, new Move(5, 3));
        board.move(0, new Move(1, 1));
        board.move(1, new Move(2, 1));
        board.move(0, new Move(1, 0));
        board.move(1, new Move(1, 4, 0, 4));
        board.move(0, new Move(6, 3, 1, 0));
        board.move(1, new Move(7, 1, 1, 4));
        
        TestDrive test = new TestDrive();
        test.testHasNetwork(board);
        test.testGetConnections(board);
        
        test.testChooseMove();
    }
}