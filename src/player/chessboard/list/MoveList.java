/* MoveList.java */

package player.chessboard.list;

import player.*;

/**
 * a class storing a list of moves.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 * 
 */
public class MoveList extends ArrayList {
    
    /**
     * a class representing the nodes in the MoveList.
     * 
     * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
     * 
     */
    class MoveListNode extends ArrayListNode {
        
        /**
         * MoveListNode() constructs a node of the MoveList.
         * 
         * @param item the move stored in the node.
         * @param prev the previous node of "this" node.
         * @param next the next node of "this" node.
         */
        protected MoveListNode(Move item, MoveListNode prev, MoveListNode next) {
            super(item, prev, next);
        }
    }
    
    /**
     * MoveList() constructs a new empty list.
     */
    public MoveList() {
        head = new MoveListNode(null, null, null);
        head.prev = head.next = head;
    }
    
    /**
     * add() adds a move to the end of the MoveList.
     * 
     * @param item the move being added.
     */
    public void add(Move item) {
        head.prev = 
        head.prev.next = new MoveListNode(item, (MoveListNode) head.prev, (MoveListNode) head);
        size ++;
    }
    
    /**
     * toArray() returns an array representation of the MoveList.
     * 
     * @return an array representation of the MoveList.
     */
    public Move[] toArray() {
        MoveListNode cur = (MoveListNode) head;
        int index = 0;
        Move[] itemList = new Move[size];
        while (cur.next != head) {
            cur = (MoveListNode) cur.next;
            itemList[index] = (Move) cur.item;
            index++;
        }
        return itemList;
    }
}