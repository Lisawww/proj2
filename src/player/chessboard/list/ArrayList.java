/* ArrayList.java */

package player.chessboard.list;

/**
 * An abstract class representing a list which has the structure of a DList and have an adding 
 * method as an ArrayList.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 *
 */
abstract class ArrayList {
    
    /**
     * An abstract class representing the nodes of an ArrayList.
     * 
     * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
     *
     */
    abstract class ArrayListNode {
        protected Object item;
        protected ArrayListNode prev, next;
        
        /**
         * ArrayListNode() constructs a node of the ArrayList.
         * 
         * @param item the object stored in the node.
         * @param prev the previous node of "this" node.
         * @param next the next node of "this" node.
         */
        protected ArrayListNode(Object item, ArrayListNode prev, ArrayListNode next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }
    
    /**
     * head the sentinel node of the ArrayList.
     * size the length of the ArrayList.
     * 
     * All fields are protected.
     */
    protected ArrayListNode head;
    protected int size = 0;
    
    /**
     * length() return the size of the ArrayList.
     * 
     * @return the size of the ArrayList.
     */
    public int length() {return size;}
    
    /**
     * toString() returns a string representation of the ArrayList.
     */
    public String toString() {
        String repr = "[ ";
        ArrayListNode cur = head;
        while (cur.next != head) {
            cur = cur.next;
            repr += cur.item + " ";
        }
        repr += "]";
        return repr;
    }
}