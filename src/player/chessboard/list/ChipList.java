/* ChipList.java */

package player.chessboard.list;
import player.chessboard.ChessBoard.Chip;

/**
 * a public class of a list. Act as an ArrayList in java.util with the structure of a 
 * DList (with sentinel).
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 */
public class ChipList extends ArrayList {
    
    /**
     * a class representing the nodes in the ChipList.
     * 
     * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
     * 
     */
    class ChipListNode extends ArrayListNode {
        
        /**
         * ChipListNode() constructs a node of the ChipList.
         * 
         * @param item the chip stored in the node.
         * @param prev the previous node of "this" node.
         * @param next the next node of "this" node.
         */
        protected ChipListNode(Chip item, ChipListNode prev, ChipListNode next) {
            super(item, prev, next);
        }
    }
    
    /**
     * ChipList() constructs a new empty list.
     */
    public ChipList() {
        head = new ChipListNode(null, null, null);
        head.prev = head.next = head;
    }
    
    /**
     * add() adds a chip to the end of the ChipList.
     * 
     * @param item the chip being added.
     */
    public void add(Chip item) {
        head.prev = 
        head.prev.next = new ChipListNode(item, (ChipListNode) head.prev, (ChipListNode) head);
        size ++;
    }

    /**
     * toArray() returns an array representation of the ChipList.
     * 
     * @return an array representation of the ChipList.
     */
    public Chip[] toArray() {
        ChipListNode cur = (ChipListNode) head;
        int index = 0;
        Chip[] itemList = new Chip[size];
        while (cur.next != head) {
            cur = (ChipListNode) cur.next;
            itemList[index] = (Chip) cur.item;
            index++;
        }
        return itemList;
    }
}