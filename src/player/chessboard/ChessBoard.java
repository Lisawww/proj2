/* ChessBoard.java */

package player.chessboard;
import player.*;
import player.chessboard.list.*;

/**
 * A class representing the internal 8 * 8 chessboard for the MachinePlayer.
 * Since it's internal, all fields are protected.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
 * 
 */
public class ChessBoard {
    
    /**
     * A class representing each square on the chessboard.
     * 
     * @author cs61b-aje / cs61b-ajg / cs61b-aiz.
     * 
     */
    public final class Chip {
        
        /**
         * isBlocked info about whether the square is forbidden for BLACK (isBlocked[0]) 
         * and WHITE (isBlocked[1]).
         * neighbours the neighbours of the same color of BLACK (neighbours[0]) and 
         * WHITE (neighbours[1]).
         * position the chip's position (x, y) on the board.
         * current the color of player occupying that square: WHITE is 1, Black is 0 
         * and empty is -1;
         * ChessBoard the board where the chips are placed.
         */
        private boolean[] isBlocked = {false, false};
        private int[] neighbours = {0, 0}, position = new int[2];
        private int current = -1;
        
        /**
         * Chip() a constructor of a new chip.
         * 
         * @param x the x-coordinate of the position of the chip on the board.
		 * @param y the y-coordinate of the position of the chip on the board.
		 * @param board the board where the chip is placed.
		 */
        private Chip(int x, int y, ChessBoard board) {
            position[0] = x;
            position[1] = y;
        }
        
        /**
         * add() adds color to a chip on the board.
         * 
         * @param color the color of the player placing the chip.
         */
        private void add(int color) {
            current = color;
            isBlocked[0] = isBlocked[1] = true;
        }
        
        /**
         * remove() resets the color of the chip.
         * 
         * @param color the color of the player removing the chip.
         */
        private void remove(int color) {
            current = -1;
            isBlocked[0] = isBlocked[1] = false;
        }
        
        /**
         * position() returns the position x or y of the chip on the board.
         * 
         * @param index x(0) or y(1) of the chip on the board.
         * @return the position x or y of the chip ont the board.
         */
        protected int position(int index) {
            return position[index];
        }
        
        /**
         * toString() returns a string representation of the chip's position (x, y).
         */
        public String toString() {
            return "(" + position[0] + ", " + position[1] + ")";
        }
    }
    
    /**
     * board represents the internal chessboard as a 2-D array.
     * player indicates the player currently using the board.
     * self records the number of chips placed by MachinePlayer on the board. 
     * oppo records the number of chips placed by the opponent on the board. 
     */
    private Chip[][] board = new Chip[8][8];
    protected MachinePlayer player;
    private int self, oppo;
    private final int SELF, OPPO;
    private ChipList[] positions = {new ChipList(), new ChipList()}, 
                       unblocked = {new ChipList(), new ChipList()};
    
    /**
     * ChessBoard() constructs a new board for a game. Called only by MachinePlayer.
     * 
     * @param player the MachinePlayer that is using the board.
     */
    public ChessBoard(MachinePlayer player) {
        this.player = player;
        SELF = player.color(); OPPO = 1 - SELF;
        
        // Initializing.
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                board[x][y] = new Chip(x, y, this);
            }
        }
        for (int i = 0; i < 8; i++) {
            board[0][i].isBlocked[0] = true;
            board[7][i].isBlocked[0] = true;
            board[i][0].isBlocked[1] = true;
            board[i][7].isBlocked[1] = true;
        }
        
        findUnblocked(SELF); findUnblocked(OPPO);
        findPositions(SELF); findPositions(OPPO);
    }
    
    /**
     * move() returns whether move m is a valid move. If yes, perform the move, else do nothing.
     * 
     * @param playerKind the kind of player: "self" or "oppo".
     * @param m the move to be performed.
     * @return whether move m is a valid move.
     */
    public boolean move(int playerKind, Move m) {
        try {
            if (m.moveKind == 1) {
                add(playerKind, m);
            } else if (m.moveKind == 2) {
                remove(playerKind, m);
                add(playerKind, m);
            }
            return true;
        } catch (IllegalMoveException e) {return false;}
    }
    
    /**
     * counterMove() cancels the effect of move m, which must be the last move performed.
     * 
     * @param playerKind the player performing the move.
     * @param m move to be cancelled (the last move).
     */
    protected void counterMove(int playerKind, Move m) {
        try {
            if (m.moveKind == 1) {
                remove(playerKind, new Move(0, 0, m.x1, m.y1));
            } else if (m.moveKind == 2) {
                move(playerKind, new Move(m.x2, m.y2, m.x1, m.y1));
            }
        } catch (IllegalMoveException e) {System.err.println("Something wrong with the code.");}
    }
    
    /**
     * add() add a chip placed by player on the board according to a move.
     * 
     * @param playerKind the type of player: "self" or "oppo".
     * @param move the move made by the player.
     * @throws IllegalMoveException
     */
    private void add(int playerKind, Move move) throws IllegalMoveException {
        Chip addTo = board[move.x1][move.y1];
        
        int color = playerKind;
        if (!(color == SELF && self < 10) && 
            !(color == OPPO && oppo < 10)) {throw new IllegalMoveException(move);}
        
        if (addTo.isBlocked[color]) {
            throw new IllegalMoveException(move);
        } else {
            addTo.add(color);
            if (color == SELF) {self++;}
            else if (color == OPPO) {oppo++;}
            
            // Block chips according to the rules.
            for (int x = move.x1 - 1; x < move.x1 + 2; x ++) {
                for (int y = move.y1 - 1; y < move.y1 + 2; y ++) {
                    if (indexInBounds(x) && indexInBounds(y)) {
                        if (!(x == move.x1 && y == move.y1)) {board[x][y].neighbours[color]++;}
                        if (board[x][y].neighbours[color] > 1) {
                            board[x][y].isBlocked[color] = true;
                        }
                        
                        if (!(x == move.x1 && y == move.y1) && board[x][y].current == color) {
                            blockNeighbours(x, y, color);
                            blockNeighbours(move.x1, move.y1, color);
                        }
                    } else {continue;}
                }
            }
        }
        
        findPositions(color); findPositions(1 - color);
        findUnblocked(color); findUnblocked(1 - color);
    }
    
    /**
     * remove() removes a chip from the board according to the step move.
     * 
     * @param playerKind the type of player removing the chip: "self" or "oppo".
     * @param move the step move made by the player.
     * @throws IllegalMoveException
     */
    private void remove(int playerKind, Move move) throws IllegalMoveException {
        Chip removeFrom = board[move.x2][move.y2];
        
        int color = playerKind;
        if (!(color == SELF && self < 11) && 
            !(color == OPPO && oppo < 11)) {throw new IllegalMoveException(move);}
        
        if (removeFrom.current != color) {
            throw new IllegalMoveException(move);
        } else {
            removeFrom.remove(color);
            if (color == SELF) {self--;}
            else if (color == OPPO) {oppo--;}
            
            // Unblock chips according to the rules.
            if (board[move.x2][move.y2].neighbours[1 - color] > 1 ||
                board[move.x2][move.y2].position[1 - color] == 0 ||
                board[move.x2][move.y2].position[1 - color] == 7) {
                board[move.x2][move.y2].isBlocked[1 - color] = true;
            } else if (board[move.x2][move.y2].neighbours[1 - color] > 0) {
                for (int x = move.x2 - 1; x < move.x2 + 2; x ++) {
                    for (int y = move.y2 - 1; y < move.y2 + 2; y ++) {
                        if (indexInBounds(x) && indexInBounds(y)) {
                            if (board[x][y].current == 1 - color && board[x][y].neighbours[1 - color] > 0) {
                                board[move.x2][move.y2].isBlocked[1 - color] = true;
                            }
                        } else {continue;}
                    }
                }
            }
            for (int x = move.x2 - 1; x < move.x2 + 2; x ++) {
                for (int y = move.y2 - 1; y < move.y2 + 2; y ++) {
                    if (indexInBounds(x) && indexInBounds(y)) {
                        if (!(x == move.x2 && y == move.y2)) {board[x][y].neighbours[color]--;}
                        if (board[x][y].current == -1 && 
                            board[x][y].position[color] != 0 && board[x][y].position[color] != 7 &&
                            board[x][y].neighbours[color] < 2) {
                            board[x][y].isBlocked[color] = false;
                            for (int i = x - 1; i < x + 2; i ++) {
                                for (int j = y - 1; j < y + 2; j++) {
                                    if (indexInBounds(i) && indexInBounds(j)) {
                                        if (board[i][j].current == color && board[i][j].neighbours[color] > 0) {
                                            board[x][y].isBlocked[color] = true;
                                        }
                                    } else {continue;}
                                }
                            }
						} else if (board[x][y].current == color) {
						    for (int i = x - 1; i < x + 2; i ++) {
						        for (int j = y - 1; j < y + 2; j++) {
						            if (indexInBounds(i) && indexInBounds(j)) {
						                if (board[i][j].current == -1 && 
						                    board[i][j].position[color] != 0 && 
						                    board[i][j].position[color] != 7 &&
						                    board[i][j].neighbours[color] < 2) {
						                    board[i][j].isBlocked[color] = false;
						                }
						            } else {continue;}
						        }
						    }
						}
                    } else {continue;}
                }
            }
        }
        
        findPositions(color); findUnblocked(color);
    }
    
    /**
     * blockNeighbours() blocks all the neighbours of a chip from placing another chips 
     * with the same color.
     * 
     * @param x the x-coordinate of the chip on the board.
     * @param y the y-coordinate of the chip on the board.
     * @param color the color of the chip at position (x, y) on the board.
     */
    private void blockNeighbours(int x, int y, int color) {
        for (int i = x - 1; i < x + 2; i ++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (indexInBounds(i) && indexInBounds(j)) {
                    board[i][j].isBlocked[color] = true;
                } else {continue;}
            }
        }
    }
    
    /**
     * indexInBounds() checks if the index is between 0 and 8.
     * 
     * @param index the index to be checked.
     * @return whether the index is in bounds.
     */
    private boolean indexInBounds(int index) {
        return (index > -1 && index < 8);
    }
    
    /**
     * findUnblocked() returns the unblocked squares of color on the board.
     * 
     * @param color the color whose unblocked squares the player wants to know about.
     * @return a list of unblocked squares of color on the board.
     */
    private void findUnblocked(int color) {
        unblocked[color] = new ChipList();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (!board[x][y].isBlocked[color]) {unblocked[color].add(board[x][y]);}
            }
        }
    }
    
    /**
     * findPositions() returns the positions of color on the board.
     * 
     * @param color the color whose positions the player wants to know about.
     * @return a list of the positions of color on the board.
     */
    private void findPositions(int color) {
        positions[color] = new ChipList();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (board[x][y].current == color) {
                    positions[color].add(board[x][y]);
                }
            }
        }
    }
    
    /**
     * unblockedAfterRemove() returns a list of unblocked chips after removing a chip according
     * to Move m.
     * 
     * @param m A move
     * @param color the color of the chips.
     * @return a list of unblocked chips after removing a chip according to Move m.
     */
    private Chip[] unblockedAfterRemove(Move m, int color) {
        Chip[] unblock = new Chip[0];
        try {
            remove(color, m);
            unblock = unblocked[color].toArray();
            add(color, new Move(m.x2, m.y2));
        } catch (IllegalMoveException e) { }
        return unblock;
    }
    
    /**
     * getConnections() returns a list of chips of the same color connected to the given 
     * chip according to rules.
     * 
     * @param chip the chip we want to get connections of.
     * @param color the color of the chip.
     * @return a list of chips of the same color connected to the given chip.
     */
    protected ChipList getConnections(Chip chip, int color) {
        int x = chip.position[0], y = chip.position[1], i, j;
        ChipList connections = new ChipList();
        
        i = x - 1; j = y;
        while (indexInBounds(i)) {
            if (board[i][j].current == -1) {i--;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x + 1;
        while (indexInBounds(i)) {
            if (board[i][j].current == -1) {i++;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x; j = y - 1;
        while (indexInBounds(j)) {
            if (board[i][j].current == -1) {j--;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        j = y + 1;
        while (indexInBounds(j)) {
            if (board[i][j].current == -1) {j++;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x - 1; j = y - 1;
        while (indexInBounds(i) && indexInBounds(j)) {
            if (board[i][j].current == -1) {i--; j--;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x - 1; j = y + 1;
        while (indexInBounds(i) && indexInBounds(j)) {
            if (board[i][j].current == -1) {i--; j++;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x + 1; j = y - 1;
        while (indexInBounds(i) && indexInBounds(j)) {
            if (board[i][j].current == -1) {i++; j--;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        
        i = x + 1; j = y + 1;
        while (indexInBounds(i) && indexInBounds(j)) {
            if (board[i][j].current == -1) {i++; j++;}
            else {
                if (board[i][j].current == color) {connections.add(board[i][j]);}
                break;
            }
        }
        return connections;
    }

    /**
     * positions() returns a list of chips of "color" on the board.
     * 
     * @param color the color of the chips.
     * @return a list of chips of "color" on the board.
     */
    protected Chip[] positions(int color) {
        return positions[color].toArray();
    }
    
    /**
     * generateMoves() generates a list of valid moves by player with color.
     * 
     * @param color the color of the player.
     * @return a list of valid moves.
     */
    protected Move[] generateMoves(int color) {
        MoveList validMoves = new MoveList();
        int num = -1;
        if (color == SELF) {num = self;}
        else if (color == OPPO) {num = oppo;}
        if (num > -1 && num < 10) {
            for (Chip chip : unblocked[color].toArray()) {
                validMoves.add(new Move(chip.position(0), chip.position(1)));
            }
        } else if (num == 10) {
            for (Chip remove : positions(color)) {
                for (Chip add : unblockedAfterRemove(new Move(0, 0,
                                                              remove.position(0), remove.position(1)), color)) {
                    if (add != remove) {
                        validMoves.add(new Move(add.position(0), add.position(1),
                                                remove.position(0), remove.position(1)));
                    }
                }
            }
        }
        return validMoves.toArray();
    }
    
    /**
     * toString() returns a string representation of the board.
     */
    public String toString() {
        String[] repr = {"| ", "| ", "| ", "| ", "| ", "| ", "| ", "| "};
        
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                if (board[x][y].current == -1) {repr[y] += "- ";}
                else {repr[y] += board[x][y].current + " ";}
            }
            repr[y] += "|";
        }
        
        return repr[0] + "\n" + repr[1] + "\n" + repr[2] + "\n" + repr[3] + "\n" + repr[4] + "\n" +
               repr[5] + "\n" + repr[6] + "\n" + repr[7] + "\n";
    }
    
    /**
     * bestMove() returns the best move according to the current status of the board. 
     * Default: random.
     * 
     * @return the best move according to the current status of the board.
     */
    public Move bestMove() {
        if (self > 2) {
            return (new GameTree(this)).selectMove();
        }
        Move[] moves = generateMoves(SELF);
        return moves[(int) (moves.length * Math.random())];
    }
}