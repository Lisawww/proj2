/* Paths.java */

package player.chessboard;
import java.util.Arrays;
import player.chessboard.ChessBoard.Chip;
import player.chessboard.list.ChipList;

/**
 * a class finding paths, containing a network identifier.
 * 
 * @author cs61b-aje / cs61b-ajg / cs61b-aiz
 */
public class Paths {
    
    private ChessBoard board;
    
    /**
     * Paths() constructs a Paths object. 
     * 
     * @param board is the chessboard being evaluated.
     */
    protected Paths(ChessBoard board) {
        this.board = board;
    }
    
    /**
     * getPath() returns if there is a path ending with an "endChip" (index 7) in goal area.
     * This is a helper function for hasNetwork();
     * 
     * @param color the color of the chips.
     * @param startChip the starting point of a path.
     * @param rest rest of the chips which are not visited.
     * @param prevDirect previous direction that cannot be repeated.
     *                   (represented by slopes, with infinite slope = 2, no direction = -2)
     * @param numChips number of chips already used to build a path.
     * @return if there is a path ending with an "endChip" (index 7) in goal area.
     */
    private boolean getPath(int color, Chip startChip, Chip[] rest, int prevDirect, int numChips) {
        for (Chip nextChip : board.getConnections(startChip, color).toArray()) {
            if (isStartChip(color, nextChip)) {continue;}
            if (numChips < 5 && isEndChip(color, nextChip)) {continue;}
            if (!Arrays.asList(rest).contains(nextChip)) {continue;}
            
            int direction;
            if (startChip.position(0) == nextChip.position(0)) {
                direction = 2;
            } else {direction = (startChip.position(1) - nextChip.position(1)) / 
                                (startChip.position(0) - nextChip.position(0));}
            if (direction != prevDirect) {
                if (isEndChip(color, nextChip)) {
                    return true;}
                
                ChipList restChips = new ChipList();
                for (Chip chip : rest) {
                    if (chip != nextChip) {
                        restChips.add(chip);
                    }
                }
                if (!getPath(color, nextChip, restChips.toArray(), direction, numChips + 1)) {
                    continue;
                } else {return true;}
            }
        }
        return false;
    }
    
    /**
     * isStartChip() returns whether a chip can be the starting chip of a network (with index 0).
     * 
     * @param color the color of the network.
     * @param chip the chip to be checked.
     * @return whether a chip can be the starting chip of a network.
     */
    private boolean isStartChip(int color, Chip chip) {
        return chip.position(1 - color) == 0;
    }
    
    /**
     * isEndChip() returns whether a chip can be the ending chip of a network (with index 7).
     * 
     * @param color the color of the network.
     * @param chip the chip to be checked.
     * @return whether a chip can be the ending chip of a network (with index 7).
     */
    private boolean isEndChip(int color, Chip chip) {
        return chip.position(1 - color) == 7;
    }
    
    /**
     * hasNetwork() checks if the path of the player can win the game.
     * 
     * @param color represents the player. Color is either 0 (black) or 1 (white).
     * @return true if the player of the color has a valid network to win.
     */
    protected boolean hasNetwork(int color) {
        Chip[] positions = board.positions(color);
        
        for (Chip startChip : positions) {
            if (!isStartChip(color, startChip)) {continue;}
            
            ChipList restChips = new ChipList();
            for (Chip chip : positions) {
                if (chip != startChip) {
                    restChips.add(chip);
                }
            }
            if (!getPath(color, startChip, restChips.toArray(), -2, 1)) {continue;}
            return true;
        }
        return false;
    }
}